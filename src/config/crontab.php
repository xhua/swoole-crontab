<?php
return array(
'mergeexcel' =>
        array(
            'taskname' => 'mergeexcel',  
            'rule' => '0,30 * * * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util merge_excel merge >> /tmp/test.log',
                    'ext' => '',
                ),
        ),

    'collectsheet' =>
        array(
            'taskname' => 'collectsheet',  
            'rule' => '0 * * * * *',
            "unique" => 0, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util collect_sheet sheet >> /tmp/test.log',
                    'ext' => '',
                ),
        ),

    'taskid1' =>
        array(
            'taskname' => 'php -i',  
            'rule' => '20 5 0 * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util email index 0 >> /tmp/test.log',
                    'ext' => '',
                ),
        ),
    'taskid2' =>
        array(
            'taskname' => 'task2',  
            'rule' => '20 5 0 * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util email index 1 >> /tmp/test.log',
                    'ext' => '',
                ),
        ),
    'taskid3' =>
        array(
            'taskname' => 'task3',  
            'rule' => '20 14 00 * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util email index 2 >> /tmp/test.log',
                    'ext' => '',
                ),
        ),
    'taskid4' =>
        array(
            'taskname' => 'task4',  
            'rule' => '20 14 00 * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util email index 3 >> /tmp/test.log',
                    'ext' => '',
                ),
        ),
'taskid5' =>
        array(
            'taskname' => 'task5',  
            'rule' => '20 20 00 * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util email index 4 >> /tmp/test.log',
                    'ext' => '',
                ),
        ),
'taskid6' =>
        array(
            'taskname' => 'task6',  
            'rule' => '20 20 00 * * *',
            "unique" => 1, 
            'execute'  => 'Cmd',
            'args' =>
                array(
                    'cmd'    => 'cd /var/www/website/dh_audi_encode;php index.php util email index 5 >> /tmp/test.log',
                    'ext' => '',
                ),
        )

);
